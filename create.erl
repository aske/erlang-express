-module(create).
-export([create/1, reverse_create/1]).

create(N) when is_integer(N), N > 0 ->
    create_aux(N, []).

create_aux(0, List) ->
    List;
create_aux(N, List) ->
    create_aux(N-1, [N | List]).


reverse_create(N) when is_integer(N), N > 0 ->
    reverse_create_aux(N, 1, []).

reverse_create_aux(N, N, List) ->
    [N | List];
reverse_create_aux(N, I, List) ->
    reverse_create_aux(N, I+1, [I | List]).  
