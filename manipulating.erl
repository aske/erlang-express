-module(manipulating).
-export([filter/2, reverse/1, concatenate/1, flatten/1]).

filter(List, Anchor) when is_integer(Anchor) ->
    filter_aux(List, Anchor, []).

filter_aux([], _, NewList) ->
    NewList;
filter_aux([Element | List], Anchor, NewList) ->
    if
        Anchor < Element ->
            filter_aux(List, Anchor, NewList);
        true              ->
            filter_aux(List, Anchor, NewList ++ [Element])
    end.
    
reverse(List) ->
    reverse_aux(List, []).

reverse_aux([], NewList) ->
    NewList;
reverse_aux([Head | Tail], NewList) ->
    reverse_aux(Tail, [Head | NewList]).

concatenate(Lists) ->
    concatenate_aux(Lists, []).

concatenate_aux([], Result) ->
    Result;
concatenate_aux([List | Rest], Result) ->
    concatenate_aux(Rest, Result ++ List).

flatten(List) ->
    reverse(flatten_aux(List, [], [])).

flatten_aux([], Result, []) ->
    Result;
flatten_aux(List, Result, [AuxHead | AuxTail]) ->
    flatten_aux(AuxTail ++ List, Result, AuxHead);
flatten_aux(List, Result, Aux) when not is_list(Aux) ->
    flatten_aux(List, [Aux | Result], []);
flatten_aux([Head | Tail], Result, _) when is_list(Head) ->
    flatten_aux(Tail, Result, Head);
flatten_aux([Head | Tail], Result, _) ->
    flatten_aux(Tail, [Head | Result], []).
