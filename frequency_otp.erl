-module(frequency_otp).
-compile(export_all).

-behaviour(gen_server).

start() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, get_frequencies(), []).

stop() ->
    gen_server:cast(?MODULE, stop).

allocate() ->
    gen_server:call(?MODULE, allocate).

deallocate(Freq) ->
    gen_server:call(?MODULE, {deallocate, Freq}).

init(FreqList) ->
    Freqs = {FreqList, []},
    {ok, Freqs}.

handle_cast(stop, Freqs) ->
    {stop, normal, Freqs}.

handle_call(allocate, From, Freqs) ->
    {NewFreqs, Reply} = allocate(Freqs, From),
    {reply, Reply, NewFreqs};
handle_call({deallocate, Freq}, _From, Freqs) ->
    NewFreqs = deallocate(Freqs, Freq),
    {reply, ok, NewFreqs}.

terminate(normal, State) ->
    ok.


allocate({[], Allocated}, _Pid) ->
    {{[], Allocated}, {error, no_frequency}};

allocate({[Freq|Free], Allocated}, Pid) ->
    {{Free, [{Freq, Pid}|Allocated]}, {ok, Freq}}.


deallocate({Free, Allocated}, Freq) ->
    {value, {Freq, Pid}} = lists:keysearch(Freq, 1, Allocated),
    NewAllocated = lists:keydelete(Freq, 1, Allocated),
    {[Freq|Free], NewAllocated}.

get_frequencies() ->
    [10, 11, 12, 13, 14, 15].
