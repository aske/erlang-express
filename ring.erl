-module(ring).
-export([start/3, loop/3]).

start(ProcNum, MsgNum, Message) when ProcNum > 0, MsgNum > 0 ->
    register(first, self()),
    first ! {spawn, 1, Message},
    first ! {message, 0, Message},
    loop(ProcNum, MsgNum, self()).

loop(ProcNum, MsgNum, NextPid) ->
    receive
        {spawn, ProcNum, _} ->
            loop(ProcNum, MsgNum, first);
        {spawn, CurrentProcNum, Message} ->
            Pid = spawn(ring, loop, [ProcNum, MsgNum, self()]),
            io:format("spawning~n"),
            Pid ! {spawn, CurrentProcNum+1, Message},
            loop(ProcNum, MsgNum, Pid);
        {message, MsgNum, _} ->
            io:format("Max number of msg reached~n"),
            NextPid ! stop,
            loop(ProcNum, MsgNum, self());
        {message, CurrentMsgNum, Message} ->
            io:format("message n. ~b~n", [CurrentMsgNum]),
            NextPid ! {message, CurrentMsgNum+1, Message},
            loop(ProcNum, MsgNum, NextPid);
        stop ->
            NextPid ! stop,
            true;
        _Other ->
            loop(ProcNum, MsgNum, NextPid)
    end.
