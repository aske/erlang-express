-module(effects).
-export([print/1, even_print/1]).

print(N) when is_integer(N), N > 0 ->
    print_iter(N, 1).

print_iter(N, N) ->
    io:format("~b~n", [N]);

print_iter(N, I) ->
    io:format("~b~n", [I]),
    print_iter(N, I+1).


even_print(N) when is_integer(N), N > 0 ->
    even_print_iter(N, 2).

even_print_iter(N, I) when I rem 2 == 0, I =< N ->
    io:format("~b~n", [I]),
    even_print_iter(N, I+2);

even_print_iter(N, I) ->
    ok.
