-module(crossring).
-export([start/3, first/4, loop/2]).

start(ProcNum, MsgNum, Message) when ProcNum > 0, MsgNum > 0 ->
    register(first, self()),
    Next1 = spawn(crossring, loop, [2, self()]),
    Next1 ! {spawn, 2, ProcNum div 2 + 1},
    Next2 = spawn(crossring, loop, [ProcNum div 2 + 2, self()]),
    Next2 ! {spawn, ProcNum div 2 + 2, ProcNum},
    Next1 ! {message, 1, Message, 1},
    first(ProcNum, MsgNum, Next1, Next2).

first(ProcNum, MsgNum, Next1, Next2) ->
    receive
        {message, MsgNum, Msg, ProcNum} ->
            io:format("Process: 1 received ~w~n", [Msg]),
            io:format("Process: 1 terminating~n"),
            Next1 ! stop,
            Next2 ! stop,
            true;
        {message, CurrentMsgNum, Msg, Sender} when Sender == (ProcNum div 2) + 1 ->
            io:format("Process: 1 received: ~w halfway through~n", [Msg]),
            Next2 ! {message, CurrentMsgNum, Msg, 1},
            first(ProcNum, MsgNum, Next1, Next2);
        {message, CurrentMsgNum, Msg, ProcNum} ->
            io:format("Process: 1 received: ~w~n", [Msg]),
            Next1 ! {message, CurrentMsgNum+1, Msg, 1},
            first(ProcNum, MsgNum, Next1, Next2);
        _Other ->
            first(ProcNum, MsgNum, Next1, Next2)
    end.
            
loop(CurProcNum, Next) ->            
    receive
        {spawn, CurrentProc, CurrentProc} ->
            loop(CurProcNum, first);
        {spawn, CurrentProc, LastProc} ->
            Pid = spawn(crossring, loop, [CurrentProc+1, self()]),
            Pid ! {spawn, CurrentProc+1, LastProc},
            loop(CurProcNum, Pid);
        {message, CurrentMsgNum, Msg, _} ->
            io:format("Process: ~b received: ~w~n", [CurProcNum, Msg]),
            Next ! {message, CurrentMsgNum, Msg, CurProcNum},
            loop(CurProcNum, Next);
        stop ->
            io:format("Process: ~b terminating~n", [CurProcNum]),
            Next ! stop,
            true;
        _Other ->
            loop(CurProcNum, Next)
    end.
