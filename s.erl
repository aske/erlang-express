-module(s).
-export([s/1, c/2, l/0]).

s(N) ->
    register(N, spawn(s, l, [])).

c(N, X) ->
    N ! {self(), X},
    receive Y ->
            Y 
    end.

l() ->
    receive
        update ->
            s:l();
        {Pid, X} ->
            Pid ! X+X,
            l()
    end.
             
