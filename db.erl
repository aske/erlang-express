-module(db).
-export([new/0, write/3, delete/2, read/2, match/2, destroy/1]).

new() ->
    [].

write(Key, Element, DbRef) ->
    [{Key, Element} | DbRef].

delete(Key, DbRef) ->
    delete_aux(Key, DbRef, []).

delete_aux(_, [], NewDbRef) ->
    NewDbRef;
delete_aux(Key, [{CurrentKey, CurrentElem} | RestDb], NewDbRef) ->
    if
        Key == CurrentKey ->
            NewDbRef ++ RestDb;
        true              ->
            delete_aux(Key, RestDb, [{CurrentKey, CurrentElem} | NewDbRef])
    end.

read(Key, DbRef) ->
    read_aux(Key, DbRef).

read_aux(_, []) ->
    {error, instance};
read_aux(Key, [{CurrentKey, CurrentElem} | RestDb]) ->
    if
        Key == CurrentKey ->
            {ok, CurrentElem};
        true              ->
            read_aux(Key, RestDb)
    end.


match(Element, DbRef) ->
    match_aux(Element, DbRef, []).

match_aux(_, [], Keys) ->
    Keys;
match_aux(Element, [{CurrentKey, CurrentElem} | RestDb], Keys) ->
    if
        Element == CurrentElem ->
            match_aux(Element, RestDb, Keys ++ [CurrentKey]);
        true                   ->
            match_aux(Element, RestDb, Keys)
    end.

destroy(_) ->
    ok.
