-module(echo).
-export([start/0, stop/0, print/1, loop/0]).

start() ->
    register(echo, spawn(echo, loop, [])),
    ok.

loop() ->
    receive
        {_, Msg} ->
            io:format("~w~n", [Msg]),
            loop();
        stop     ->
            true;
        _Other ->
            loop()
    end.

stop() ->
    echo ! stop,
    ok.

print(Term) ->
    echo ! {self(), Term},
    ok.
