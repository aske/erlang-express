-module(sorting).
-export([quicksort/1, mergesort/1]).

quicksort([]) ->
    [];
quicksort([Head | Tail]) ->
    Less = lists:filter(fun(X) -> X =< Head end, Tail),
    Greater = lists:filter(fun(X) -> X > Head end, Tail),
    quicksort(Less) ++ [Head] ++ quicksort(Greater).   
    
mergesort(List) when length(List) =< 1 ->
    List;
mergesort(List) ->
    {Left, Right} = lists:split(length(List) div 2, List),
    lists:merge(mergesort(Right), mergesort(Left)).
